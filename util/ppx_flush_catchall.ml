(* Otags III
 * 
 * Hendrik Tews Copyright (C) 2010 - 2017
 * 
 * This file is part of "Otags III".
 * 
 * "Otags III" is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * "Otags III" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License in file COPYING in this or one of the parent
 * directories for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with "Otags III". If not, see
 * <http://www.gnu.org/licenses/>.
 * 
 * ppx preprocessor for deleting last catch-all match cases
 * 
 *)

open Longident
open Asttypes
open Parsetree
open Ast_mapper

let rec list_split_last = function
  | [] -> assert false
  | [x] -> ([], x)
  | x::l ->
     let (rest, last) = list_split_last l in
     (x::rest, last)

type catch_all_error =
  | Only_one_pattern
  | No_catch_all
  | No_matching

let only_one_pattern =
  "misplaced @flushcatchall - cannot delete the only match case"

let no_catch_all =
  "misplaced @flushcatchall - no catchall match case found"

let no_matching =
  "misplaced @flushcatchall - there is no pattern matching here"

let string_of_catch_all_err = function
  | Only_one_pattern -> only_one_pattern
  | No_catch_all -> no_catch_all
  | No_matching -> no_matching

exception Catch_all_error of catch_all_error

(** compile an attribute that cases the compiler to emit a warnign *)
let make_warning attr_loc err =
  let warning = string_of_catch_all_err err in
  ({txt = "ocaml.ppwarning"; loc = attr_loc},
   PStr [{pstr_desc =
	    Pstr_eval(
		{pexp_desc = Pexp_constant(Pconst_string(warning, None));
		 pexp_loc = attr_loc;
		 pexp_attributes = [];
		},
		[]
	      );
	  pstr_loc = attr_loc}]
  )


(** check whether there is a @flushcatchall attribute *)
let rec check_catchall_attr = function
  | [] -> (None, [])
  | ({txt = "flushcatchall"; loc}, PStr []) :: rest ->
     (Some loc, rest)
  | attr :: attrs ->
     let (loc_opt, rest) = check_catchall_attr attrs in
     (loc_opt, attr :: rest)

(** or patterns are compiled into nested Ppat_or's. This function
    splits off the last/rightmost pattern in an Ppat_or chain
 *)
let rec split_last_or_pattern = function
  | Ppat_or(left, ({ppat_desc = Ppat_or(_, _) as right_desc; _} as right)) ->
     let (rest_or, last) = split_last_or_pattern right_desc in
     (Ppat_or(left, {right with ppat_desc = rest_or}), last)
  | Ppat_or(left, right) -> (left.ppat_desc, right.ppat_desc)
  | _ -> assert false

(** delete the last catch-all match of the form _ -> () in case_list 
    and return the remaining match cases. Raise Catch_all_error if it 
    cannot find such a catch-all.
 *)
let flush_catch_all case_list =
  let (cases, catch_all) = list_split_last case_list in
  match catch_all with
    | {pc_lhs = {ppat_desc = Ppat_any; _};
       pc_guard = None;
       pc_rhs = {pexp_desc = Pexp_construct({txt = Lident "()"; _}, None); _}
      } ->
       if cases = []
       then raise(Catch_all_error Only_one_pattern)
       else cases
    | {pc_lhs = {ppat_desc = Ppat_or(_, _) as pat_desc; _};
       pc_guard = None;
       pc_rhs = {pexp_desc = Pexp_construct({txt = Lident "()"; _}, None); _}
      } ->
       let (rest_or, last_or) = split_last_or_pattern pat_desc in
       (match last_or with
	  | Ppat_any ->
	     cases @ [{catch_all with
			pc_lhs = {catch_all.pc_lhs with
				   ppat_desc = rest_or}}]
	  | _ -> raise(Catch_all_error No_catch_all)
       )
    | _ -> raise(Catch_all_error No_catch_all)

(** Mapper that is mostly the identity on the syntax tree, but deletes 
    the last catch-all in function or match ... with pattern matches 
    that have a @flushcatchall attribute.
 *)
let flush_catchall_mapper _argv =
  {default_mapper with
    expr =
      fun mapper expr ->
      let (catchall_loc_opt, attributes) =
	check_catchall_attr expr.pexp_attributes in
      let expr =
	match catchall_loc_opt with
	  | None -> expr
	  | Some loc ->
	     try
	       match expr.pexp_desc with
		 | Pexp_function matching ->
		    {expr with
		      pexp_desc = Pexp_function(flush_catch_all matching);
		      pexp_attributes = attributes}
		 | Pexp_match(oexp, matching) ->
		    {expr with
		      pexp_desc = Pexp_match(oexp, flush_catch_all matching);
		      pexp_attributes = attributes}
		 | _ ->
		    raise (Catch_all_error No_matching)
	     with
	       | Catch_all_error err ->
		  {expr with
		    pexp_attributes = (make_warning loc err) :: attributes}
      in
      default_mapper.expr mapper expr
  }

let () =
  register "flush_catchall_mapper" flush_catchall_mapper


(*** 
 * Local Variables:
 * compile-command: "ocamlopt.opt -w A-4 -I +compiler-libs -o ppx_flush_catchall ocamlcommon.cmxa ppx_flush_catchall.ml"
 * End:
 ***)
