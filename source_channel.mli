(* Otags III
 * 
 * Hendrik Tews Copyright (C) 2010 - 2017
 * 
 * This file is part of "Otags III".
 * 
 * "Otags III" is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * "Otags III" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License in file COPYING in this or one of the parent
 * directories for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with "Otags III". If not, see
 * <http://www.gnu.org/licenses/>.
 * 
 * hold the in_channel of the source file
 * 
 *)


val reset : unit -> unit

(**
 * Return a channel for the file in the (start_pos in the) location.
 * The read position in this file might be at an arbitray point.
 * Argument primary_file must be true precisely for those input files
 * that are parsed and tagged.
 *)
val get_channel : ?primary_file:bool -> Location.t -> in_channel
    

(* XXX needed? *)
val full_string_of_loc : Location.t -> string
