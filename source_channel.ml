(* Otags III
 * 
 * Hendrik Tews Copyright (C) 2010 - 2017
 * 
 * This file is part of "Otags III".
 * 
 * "Otags III" is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * "Otags III" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License in file COPYING in this or one of the parent
 * directories for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with "Otags III". If not, see
 * <http://www.gnu.org/licenses/>.
 * 
 * hold the in_channel of the source file
 * 
 *)


open Otags_misc
open Otags_types			(* for Otags_parsing_error *)

let last_primary_file = ref ""

let current_file_name = ref ""

let current_in_channel = ref stdin

let reset () =
  if !current_file_name <> "" 
  then close_in !current_in_channel;
  current_file_name := ""

let open_file ?(primary_file = false) file_name loc =
  if primary_file
  then last_primary_file := file_name;
  reset ();
  current_in_channel := 
    (try
       open_in file_name
     with
       | Sys_error sys_msg -> 
	 let msg = 
	   if primary_file
	   then "Cannot open " ^ sys_msg
	   else
             Printf.sprintf
               ("In file %s, character position %d: " ^^
                  "Original source not available: %s")
               !last_primary_file
               loc.Location.loc_start.Lexing.pos_cnum
               sys_msg
	 in
	 raise(Otags_parsing_error(loc, msg))
    );
  current_file_name := file_name


let get_channel ?primary_file loc =
  if file_of_loc loc  <> !current_file_name
  then open_file ?primary_file (file_of_loc loc) loc;
  !current_in_channel
    


let full_string_of_loc (loc : Location.t) =
  Printf.sprintf "[%s: sl %d(%d) so %d el %d(%d) eo %d%s]"
    loc.Location.loc_start.Lexing.pos_fname
    loc.Location.loc_start.Lexing.pos_lnum
    loc.Location.loc_start.Lexing.pos_bol
    loc.Location.loc_start.Lexing.pos_cnum
    loc.Location.loc_end.Lexing.pos_lnum
    loc.Location.loc_end.Lexing.pos_bol
    loc.Location.loc_end.Lexing.pos_cnum
    (if loc.Location.loc_ghost then " GHOST" else " REAL")
